Rails.application.routes.draw do
  root 'artists#index'

  get 'search',  to: 'search#index'
  get 'suggest', to: 'search#suggest'

  resources :artists
end
