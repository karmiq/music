if Rails.env.development?
  tracer = ActiveSupport::Logger.new(STDERR)
  tracer.level =  Logger::DEBUG
  tracer.formatter = proc { |s, d, p, m| "\e[2m#{m}\n\e[0m" }
  # Elasticsearch::Persistence.client.transport.tracer = tracer
end
